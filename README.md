# Projeto Portifólio API - Documentação do Desenvolvedor

Bem-vindo ao Projeto Portifólio API! Este é um guia rápido para entender como utilizar nossa API para gerenciar projetos de forma eficiente. Se você é um desenvolvedor, está no lugar certo para começar a explorar os endpoints disponíveis.

## Como Usar a API

A API é projetada para ser fácil de usar e oferece operações básicas para gerenciar os projetos e associar os membros aos projetos. Aqui estão os principais pontos:

### 1. Busca de Projeto por ID

**Endpoint:**
```http
GET /projetos/{id}
```

**Descrição:**
Este endpoint permite buscar informações detalhadas sobre um projeto específico com base no ID.

### 2. Criação de Projeto

**Endpoint:**
```http
POST /projetos
```

**Descrição:**
Crie um projeto enviando os dados necessários no corpo da solicitação. Após a criação, você receberá os detalhes do projeto recém-criado.

### 3. Exclusão de Projeto por ID

**Endpoint:**
```http
DELETE /projetos/{id}
```

**Descrição:**
Exclua um projeto com base no ID fornecido. Após a exclusão, o projeto não estará mais disponível na API.
Porém, se o projeto estiver em um dos status mencionados abaixo, não poderá ser excluído.

 - INICIADO
 - EM ANDAMENTO
 - ENCERRADO

### 4. Atualização de Projeto por ID

**Endpoint:**
```http
PUT /projetos/{id}
```

**Descrição:**
Atualize um projeto existente com base no ID fornecido e nos dados fornecidos no corpo da solicitação. Após a atualização, você receberá os detalhes do projeto atualizado.

### 5. Associação de Membro a Projeto

**Endpoint:**
```http
POST /projetos/membros
```

**Descrição:**
Associe um membro a um projeto, fornecendo os dados necessários no corpo da solicitação. Após a associação, você receberá os detalhes do membro associado.

- Só é possível associar uma pessoa que seja funcionário
- Só é possível associar o funcionário, caso ele não tenha associação para aquele projeto

## Executando o Projeto Localmente

Para começar a usar a API, siga estas etapas simples:

1. **Instale o docker e execute o script abaixo para subir uma instancia do postgres local:**
   ```bash
   docker run --name portifolio-database -e POSTGRES_PASSWORD=admin -p 5432:5432 -d postgres
   ```

2.**Clone o Repositório:**
   ```bash
   git clone https://gitlab.com/victoraraujodev/ms-portifolio.git
   cd ms-portifolio
   ```

3.**Execute o Projeto:**
   ```bash
   ./mvnw spring-boot:run
   ```