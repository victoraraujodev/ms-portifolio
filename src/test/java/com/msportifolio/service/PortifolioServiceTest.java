package com.msportifolio.service;


import com.msportifolio.adapter.PortifolioAdapter;
import com.msportifolio.entity.GerenteEntity;
import com.msportifolio.entity.MembroEntity;
import com.msportifolio.entity.PessoaEntity;
import com.msportifolio.entity.ProjetoEntity;
import com.msportifolio.enums.RiscoEnum;
import com.msportifolio.enums.StatusEnum;
import com.msportifolio.model.Membro;
import com.msportifolio.model.Projeto;
import com.msportifolio.repository.MembroRepository;
import com.msportifolio.repository.PessoaRepository;
import com.msportifolio.repository.ProjetoRepository;
import jakarta.persistence.NoResultException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PortifolioServiceTest {

    @Mock
    private ProjetoRepository projetoRepository;

    @Mock
    private MembroRepository membroRepository;

    @Mock
    private PessoaRepository pessoaRepository;

    @Mock
    private PortifolioAdapter adapter;

    @InjectMocks
    private PortifolioService portifolioService;

    @Test
    void testSalvarProjeto() {

        Projeto projeto = Projeto.builder()
                .id(1L)
                .nome("Projeto Auditoria")
                .descricao("Projeto de Portifolio para a Code Group")
                .risco(RiscoEnum.MEDIO_RISCO)
                .idGerente(1L)
                .dataPrevisaoFim(LocalDate.now())
                .dataTermino(LocalDate.now())
                .status(StatusEnum.EM_ANDAMENTO)
                .orcamento(new BigDecimal(10000))
                .build();

        ProjetoEntity entity = ProjetoEntity.builder()
                .id(1L)
                .dataInicio(LocalDate.now())
                .dataPrevisaoFim(LocalDate.now())
                .risco("Baixo Risco")
                .status("Analise Realizada")
                .orcamento(new BigDecimal(10000))
                .gerenteResponsavel(GerenteEntity.builder().id(1L).build())
                .build();

        when(adapter.projetoModelToEntity(null, projeto)).thenReturn(entity);
        when(adapter.projetoEntityToModel(entity)).thenReturn(projeto);
        when(projetoRepository.save(any())).thenReturn(entity);

        Projeto result = portifolioService.salvarProjeto(projeto);

        verify(projetoRepository).save(any());
        verify(adapter).projetoEntityToModel(any());
        assertNotNull(result);
    }

    @Test
    void testAtualizarProjeto() {

        Projeto projeto = Projeto.builder()
                .id(1L)
                .nome("Projeto Auditoria")
                .descricao("Projeto de Portifolio para a Code Group")
                .risco(RiscoEnum.MEDIO_RISCO)
                .idGerente(1L)
                .dataPrevisaoFim(LocalDate.now())
                .dataTermino(LocalDate.now())
                .status(StatusEnum.EM_ANDAMENTO)
                .orcamento(new BigDecimal(20000))
                .build();

        ProjetoEntity entidade = ProjetoEntity.builder()
                .id(1L)
                .dataInicio(LocalDate.now())
                .dataPrevisaoFim(LocalDate.now())
                .risco("Baixo Risco")
                .status("Analise Realizada")
                .orcamento(new BigDecimal(10000))
                .gerenteResponsavel(GerenteEntity.builder().id(1L).build())
                .build();

        when(adapter.projetoModelToEntity(1L, projeto)).thenReturn(entidade);
        when(adapter.projetoEntityToModel(entidade)).thenReturn(projeto);
        when(projetoRepository.findById(any())).thenReturn(Optional.of(entidade));
        when(projetoRepository.save(any())).thenReturn(entidade);

        Projeto result = portifolioService.atualizarProjeto(1L, projeto);

        verify(projetoRepository).save(any());
        verify(projetoRepository).findById(1L);
        assertNotNull(result);
    }

    @Test
    void testBuscarProjetoPorId() {

        Projeto projeto = Projeto.builder()
                .id(1L)
                .nome("Projeto Auditoria")
                .descricao("Projeto de Portifolio para a Code Group")
                .risco(RiscoEnum.MEDIO_RISCO)
                .idGerente(1L)
                .dataPrevisaoFim(LocalDate.now())
                .dataTermino(LocalDate.now())
                .status(StatusEnum.EM_ANDAMENTO)
                .orcamento(new BigDecimal(20000))
                .build();

        ProjetoEntity entidade = ProjetoEntity.builder()
                .id(1L)
                .dataInicio(LocalDate.now())
                .dataPrevisaoFim(LocalDate.now())
                .nome("Projeto Auditoria")
                .risco("Baixo Risco")
                .status("Analise Realizada")
                .orcamento(new BigDecimal(10000))
                .gerenteResponsavel(GerenteEntity.builder().id(1L).build())
                .build();

        when(projetoRepository.findById(any())).thenReturn(Optional.of(entidade));
        when(adapter.projetoEntityToModel(entidade)).thenReturn(projeto);

        Projeto result = portifolioService.buscarPorId(any());

        assertNotNull(result);
        assertEquals("Projeto Auditoria", result.getNome());

    }

    @Test
    void testBuscarProjetoPorIdInexistente() {

        Long id = 1L;
        when(projetoRepository.findById(id)).thenReturn(Optional.empty());

        NoResultException exception = assertThrows(
                NoResultException.class,
                () -> portifolioService.buscarPorId(id)
        );

        assertEquals("Projeto não encontrado", exception.getMessage());

        verify(projetoRepository).findById(id);
        verify(adapter, never()).projetoEntityToModel(any());

    }

    @Test
    void testDeletarProjetoInexistente() {

        Long id = 1L;
        when(projetoRepository.findById(id)).thenReturn(Optional.empty());

        NoResultException exception = assertThrows(
                NoResultException.class,
                () -> portifolioService.deletar(id)
        );

        assertEquals("Projeto não encontrado", exception.getMessage());

        verify(projetoRepository).findById(id);
        verify(adapter, never()).projetoEntityToModel(any());

    }

    @Test
    void testDeletarProjetoComStatusNaoPermitidos() {
        Long id = 1L;

        ProjetoEntity entidade = ProjetoEntity.builder()
                .id(1L)
                .dataInicio(LocalDate.now())
                .dataPrevisaoFim(LocalDate.now())
                .nome("Projeto Auditoria")
                .risco("Baixo Risco")
                .status("EM ANDAMENTO")
                .orcamento(new BigDecimal(10000))
                .gerenteResponsavel(GerenteEntity.builder().id(1L).build())
                .build();

        when(projetoRepository.findById(any())).thenReturn(Optional.of(entidade));

        IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> portifolioService.deletar(id)
        );

        assertEquals("Projeto com o status EM ANDAMENTO não pode ser excluído", exception.getMessage());

        verify(projetoRepository).findById(id);
        verify(adapter, never()).projetoEntityToModel(any());

    }


    @Test
    void testAssociarMembroAoProjetoJaAssociado() {

        Membro membro = Membro.builder().idProjeto(1L).idPessoa(2L).build();
        MembroEntity membroEntity = MembroEntity.builder()
                .id(1L)
                .pessoa(PessoaEntity.builder()
                        .ehFuncionario(Boolean.TRUE).id(2L).build())
                .projeto(ProjetoEntity.builder().id(1L)
                        .build())
                .build();

        when(pessoaRepository.findById(any())).thenReturn(Optional.of(PessoaEntity.builder().id(2L)
                .ehFuncionario(true).build()));
        when(membroRepository.findMembrosByIdPessoa(2L)).thenReturn(List.of(membroEntity));

        IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> portifolioService.associarMembroAoProjeto(membro)
        );

        assertEquals("Membro já associado ao projeto 1", exception.getMessage());

    }

    @Test
    void testAssociarMembroAoProjetoQueNaoSejaFuncionario() {

        Membro membro = Membro.builder().idProjeto(1L).idPessoa(2L).build();

        when(pessoaRepository.findById(any())).thenReturn(Optional.of(PessoaEntity.builder().id(2L)
                .ehFuncionario(Boolean.FALSE).build()));

        IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> portifolioService.associarMembroAoProjeto(membro)
        );

        assertEquals("Associação só é permitida a funcionários", exception.getMessage());

    }

}

