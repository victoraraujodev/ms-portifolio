package com.msportifolio.model;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@Builder
public class Membro {

    private Long id;

    @NotNull(message = "idProjeto é obrigatório")
    private Long idProjeto;

    @NotNull(message = "idPessoa é obrigatório")
    private Long idPessoa;

}
