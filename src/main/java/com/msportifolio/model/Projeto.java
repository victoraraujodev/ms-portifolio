package com.msportifolio.model;

import com.msportifolio.enums.RiscoEnum;
import com.msportifolio.enums.StatusEnum;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
public class Projeto {

    private Long id;
    private String nome;
    private LocalDate dataInicio;
    private Long idGerente;
    private LocalDate dataPrevisaoFim;
    private LocalDate dataTermino;
    private BigDecimal orcamento;
    private String descricao;
    private StatusEnum status;
    private RiscoEnum risco;

}
