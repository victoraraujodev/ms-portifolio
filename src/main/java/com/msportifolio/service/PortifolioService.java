package com.msportifolio.service;


import com.msportifolio.adapter.PortifolioAdapter;
import com.msportifolio.entity.MembroEntity;
import com.msportifolio.entity.PessoaEntity;
import com.msportifolio.entity.ProjetoEntity;
import com.msportifolio.enums.StatusEnum;
import com.msportifolio.model.Membro;
import com.msportifolio.model.Projeto;
import com.msportifolio.repository.MembroRepository;
import com.msportifolio.repository.PessoaRepository;
import com.msportifolio.repository.ProjetoRepository;
import jakarta.persistence.NoResultException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PortifolioService {
    private final ProjetoRepository projetoRepository;
    private final MembroRepository membroRepository;
    private final PessoaRepository pessoaRepository;
    private final PortifolioAdapter adapter;

    public PortifolioService(ProjetoRepository projetoRepository, MembroRepository membroRepository, PessoaRepository pessoaRepository, PortifolioAdapter adapter) {
        this.projetoRepository = projetoRepository;
        this.membroRepository = membroRepository;
        this.pessoaRepository = pessoaRepository;
        this.adapter = adapter;
    }

    public Projeto salvarProjeto(Projeto projeto) {
        return adapter.projetoEntityToModel(projetoRepository.save(adapter.projetoModelToEntity(null, projeto)));
    }

    public Projeto atualizarProjeto(Long id, Projeto projeto) {
        buscarPorId(id);
        return adapter.projetoEntityToModel(projetoRepository.save(adapter.projetoModelToEntity(id, projeto)));
    }

    public Projeto buscarPorId(Long id) {
        ProjetoEntity response = projetoRepository.findById(id)
                .orElseThrow(() -> new NoResultException("Projeto não encontrado"));

        return adapter.projetoEntityToModel(response);
    }

    public void deletar(Long id) {
        Optional<ProjetoEntity> projeto = projetoRepository.findById(id);
        ProjetoEntity response = projeto.orElseThrow(() -> new NoResultException("Projeto não encontrado"));
        validaExclusao(response);
        projetoRepository.delete(response);
    }

    public Membro associarMembroAoProjeto(Membro membro) {

        verificaFuncionario(membro.getIdPessoa());

        List<MembroEntity> membroEntity = membroRepository.findMembrosByIdPessoa(membro.getIdPessoa());

        membroEntity.forEach(membroEnt -> {
            if (membroEnt.getProjeto().getId().equals(membro.getIdProjeto())) {
                throw new IllegalArgumentException(
                        String.format("Membro já associado ao projeto %s", membro.getIdProjeto())
                );
            }
        });
        return adapter.membroEntityToModel(membroRepository.save(adapter.membroModelToEntity(membro)));
    }

    private void validaExclusao(ProjetoEntity projeto) {
        List<String> statusNaoPermitidos = List.of(
                StatusEnum.INICIADO.getNome(),
                StatusEnum.EM_ANDAMENTO.getNome(),
                StatusEnum.ENCERRADO.getNome()
        );

        if (statusNaoPermitidos.contains(projeto.getStatus())) {
            throw new IllegalArgumentException(
                    String.format("Projeto com o status %s não pode ser excluído", projeto.getStatus())
            );
        }
    }

    private void verificaFuncionario(Long idPessoa) {

        Optional<PessoaEntity> pessoa = pessoaRepository.findById(idPessoa);
        PessoaEntity response = pessoa.orElseThrow(() -> new NoResultException("Pessoa não encontrada"));

        if (Boolean.FALSE.equals(response.getEhFuncionario())) {
            throw new IllegalArgumentException(
                    "Associação só é permitida a funcionários"
            );
        }

    }

}
