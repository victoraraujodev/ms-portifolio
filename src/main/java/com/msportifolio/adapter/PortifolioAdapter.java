package com.msportifolio.adapter;

import com.msportifolio.entity.GerenteEntity;
import com.msportifolio.entity.MembroEntity;
import com.msportifolio.entity.PessoaEntity;
import com.msportifolio.entity.ProjetoEntity;
import com.msportifolio.enums.RiscoEnum;
import com.msportifolio.enums.StatusEnum;
import com.msportifolio.model.Membro;
import com.msportifolio.model.Projeto;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;


@Component
public class PortifolioAdapter {

    public ProjetoEntity projetoModelToEntity(Long id, Projeto projeto) {
        ProjetoEntity projetoEntity = ProjetoEntity.builder().build();
        BeanUtils.copyProperties(projeto, projetoEntity);
        projetoEntity.setId(id);
        projetoEntity.setStatus(projeto.getStatus().name());
        projetoEntity.setRisco(projeto.getRisco().name());
        projetoEntity.setGerenteResponsavel(GerenteEntity.builder().id(projeto.getIdGerente()).build());
        return projetoEntity;
    }

    public Projeto projetoEntityToModel(ProjetoEntity projetoEntity) {
        Projeto projeto = Projeto.builder().build();
        BeanUtils.copyProperties(projetoEntity, projeto);
        projeto.setStatus(StatusEnum.valueOf(projetoEntity.getStatus()));
        projeto.setRisco(RiscoEnum.valueOf(projetoEntity.getRisco()));
        projeto.setIdGerente(projetoEntity.getGerenteResponsavel().getId());
        return projeto;
    }

    public MembroEntity membroModelToEntity(Membro membro) {
        return MembroEntity.builder()
                .projeto(ProjetoEntity.builder()
                        .id(membro.getIdProjeto())
                        .build())
                .pessoa(PessoaEntity.builder()
                        .id(membro.getIdPessoa())
                        .build())
                .build();
    }

    public Membro membroEntityToModel(MembroEntity membroEntity) {
        Membro membro = Membro.builder().build();
        membro.setId(membroEntity.getId());
        membro.setIdPessoa(membroEntity.getPessoa().getId());
        membro.setIdProjeto(membroEntity.getProjeto().getId());
        return membro;
    }
}
