package com.msportifolio.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Entity
@Table(name = "gerente", schema = "prt")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GerenteEntity {

    @Id
    private Long id;

    @OneToOne
    @JoinColumn(name = "id_pessoa", unique = true)
    private PessoaEntity pessoa;

    @Column(name = "cargo")
    private String cargo;

    @Column(name = "salario")
    private BigDecimal salario;

}
