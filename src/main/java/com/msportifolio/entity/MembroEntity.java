package com.msportifolio.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "membro", schema = "prt")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MembroEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_projeto")
    private ProjetoEntity projeto;

    @OneToOne
    @JoinColumn(name = "id_pessoa")
    private PessoaEntity pessoa;

}
