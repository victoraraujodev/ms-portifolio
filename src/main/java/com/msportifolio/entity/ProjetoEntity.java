package com.msportifolio.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "projeto", schema = "prt")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProjetoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nome")
    private String nome;

    @Column(name = "data_inicio")
    private LocalDate dataInicio;

    @Column(name = "data_previsao_fim")
    private LocalDate dataPrevisaoFim;

    @Column(name = "data_fim")
    private LocalDate dataTermino;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "status")
    private String status;

    @Column(name = "risco")
    private String risco;

    @Column(name = "orcamento")
    private BigDecimal orcamento;

    @ManyToOne
    @JoinColumn(name = "id_gerente")
    private GerenteEntity gerenteResponsavel;

}
