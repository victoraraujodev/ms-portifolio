package com.msportifolio.controller;


import com.msportifolio.model.Membro;
import com.msportifolio.model.Projeto;
import com.msportifolio.service.PortifolioService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@Validated
public class PortifolioController {

    private final PortifolioService service;

    public PortifolioController(PortifolioService service) {
        this.service = service;
    }

    @GetMapping("/projetos/{id}")
    public ResponseEntity<Projeto> buscaProjeto(@PathVariable("id") Long id) {
        return ResponseEntity.ok(service.buscarPorId(id));
    }

    @PostMapping("/projetos")
    public ResponseEntity<Projeto> salva(@RequestBody Projeto projeto) {

        Projeto projetoSalvo = service.salvarProjeto(projeto);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(projetoSalvo.getId())
                .toUri();

        return ResponseEntity.created(location).body(projetoSalvo);
    }

    @DeleteMapping("/projetos/{id}")
    public ResponseEntity<Void> exclui(@PathVariable("id") Long id) {
        service.deletar(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping("/projetos/{id}")
    public ResponseEntity<Projeto> atualiza(@PathVariable("id") Long id, @RequestBody Projeto projeto) {
        return ResponseEntity.ok().body(service.atualizarProjeto(id, projeto));
    }

    @PostMapping("/projetos/membros")
    public ResponseEntity<Membro> associaMembro(@Valid @RequestBody Membro membro) {

        Membro membroAssociado = service.associarMembroAoProjeto(membro);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(membroAssociado.getId())
                .toUri();

        return ResponseEntity.created(location).body(membroAssociado);
    }
}
