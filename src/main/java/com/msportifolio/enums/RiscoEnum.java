package com.msportifolio.enums;

public enum RiscoEnum {
    BAIXO_RISCO("BAIXO RISCO"),
    MEDIO_RISCO("MÉDIO RISCO"),
    ALTO_RISCO("ALTO RISCO");
    private final String nome;

    RiscoEnum(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }
}
