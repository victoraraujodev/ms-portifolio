package com.msportifolio.enums;

public enum StatusEnum {
    ANALISE("ANÁLISE"),
    ANALISE_REALIZADA("ANÁLISE REALIZADA"),
    ANALISE_APROVADA("ANÁLISE APROVADA"),
    INICIADO("INICIADO"),
    PLANEJADO("PLANEJADO"),
    EM_ANDAMENTO("EM ANDAMENTO"),
    ENCERRADO("ENCERRADO"),
    CANCELADO("CANCELADO");

    private final String nome;

    StatusEnum(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }
}
