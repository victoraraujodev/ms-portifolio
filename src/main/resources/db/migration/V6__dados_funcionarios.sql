-- Inserção de dados na tabela prt.pessoa
INSERT INTO prt.pessoa (nome, data_nascimento, cpf, funcionario)
VALUES
    ('João Silva', '1990-01-15', '123.456.789-01', true),
    ('Maria Oliveira', '1985-07-20', '987.654.321-00', true),
    ('Carlos Santos', '1995-03-10', '456.789.012-34', true);


-- Inserção de dados na tabela prt.gerente
INSERT INTO prt.gerente (id_pessoa, cargo, salario)
VALUES
    (1, 'Gerente de Vendas', 8000.00),
    (2, 'Gerente de Marketing', 8500.00),
    (3, 'Gerente de TI', 9000.00);
