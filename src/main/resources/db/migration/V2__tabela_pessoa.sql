CREATE TABLE IF NOT EXISTS prt.pessoa (
    id bigserial PRIMARY KEY,
    nome character varying(100) not NULL,
    data_nascimento date not null,
    cpf character varying(14) unique not NULL,
    funcionario boolean default true
);

CREATE INDEX IF NOT EXISTS idx_cpf ON prt.pessoa (cpf);
DO $$
BEGIN
    IF NOT EXISTS (SELECT 1 FROM pg_constraint WHERE conname = 'uk_cpf' AND conrelid = 'prt.pessoa'::regclass) THEN
        ALTER TABLE prt.pessoa ADD CONSTRAINT uk_cpf UNIQUE (cpf);
    END IF;
END $$;

ALTER TABLE prt.pessoa DROP CONSTRAINT IF EXISTS chk_data_nascimento;
ALTER TABLE prt.pessoa ADD CONSTRAINT chk_data_nascimento CHECK (data_nascimento <= CURRENT_DATE);

ALTER TABLE prt.pessoa DROP CONSTRAINT IF EXISTS chk_tamanho_nome;
ALTER TABLE prt.pessoa ADD CONSTRAINT chk_tamanho_nome CHECK (length(nome) <= 100);
