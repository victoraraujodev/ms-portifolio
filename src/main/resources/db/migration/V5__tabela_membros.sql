create table if not exists prt.membro (
    id SERIAL primary key,
    id_projeto bigint not null,
    id_pessoa bigint not null,
    data_criacao TIMESTAMP DEFAULT now(),
    constraint fk_membro_projeto foreign key (id_projeto)
        references prt.projeto (id) match simple
        on
update
	no action
        on
	delete
		no action,
		constraint fk_membro_pessoa foreign key (id_pessoa)
        references prt.pessoa (id) match simple
        on
		update
			no action
        on
			delete
				no action
);