CREATE TABLE prt.gerente (
    id SERIAL PRIMARY KEY,
    id_pessoa INTEGER NOT NULL,
    cargo VARCHAR(50) NOT NULL,
    salario NUMERIC NOT NULL,
    CONSTRAINT fk_pessoa_gerente FOREIGN KEY (id_pessoa) REFERENCES prt.pessoa (id) ON DELETE CASCADE
);

