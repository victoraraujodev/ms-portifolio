CREATE TABLE IF NOT EXISTS prt.projeto (
    id SERIAL PRIMARY KEY,
    nome VARCHAR(200) NOT NULL,
    data_inicio DATE,
    data_previsao_fim DATE,
    data_fim DATE,
    descricao VARCHAR(5000),
    status VARCHAR(200),
    risco VARCHAR(200),
    orcamento NUMERIC,
    id_gerente bigserial NOT NULL,
    CONSTRAINT fk_gerente FOREIGN KEY (id_gerente)
        REFERENCES prt.gerente (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);